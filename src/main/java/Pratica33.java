import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {
    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;

        Matriz somada = orig.soma(orig);
        Matriz transposta = orig.getTransposta();
        Matriz multiplicada = orig.prod(transposta);
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz somada: " + somada);
        System.out.println("Matriz multiplicada: " + multiplicada);
    }
}
